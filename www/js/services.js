var localStorage = localStorage;
angular.module('app.services', [])

.service('Api', function ($http, $httpParamSerializerJQLike, $q, DateFormat, $rootScope){
        var api = this;
        function send(url,method,params) {
            if(method == 'POST'){
                return $http({
                    method: 'POST',
                    url: url,
                    data: $httpParamSerializerJQLike(params),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
            }else if(method == "PUT"){
                console.log(method);
                return $http({
                    method: 'PUT',
                    url: url,
                    headers: {"Content-Type": "application/json;charset=UTF-8"},
                    data: params
                });
            }else{
                return $http ({
                    method: method,
                    url: url ,
                    params: (params==''|| params==undefined ? '' : params)
                });
            }
        }

        function getUrl(action,method, params) {
            return send(api_url + action, method, params);
        }

        function getUrlForId(action,method,params) {
            return send(getUrl(api_url + action),method,params);
        }

        api.apiUrl = function () {
            return api_url;
        }

        api.getAll = function (action, params) {
            return getUrl(action,'GET', params);
        };

        api.fetch = function (action,params) {
            return getUrlForId(action,'GET',params);
        };


        // crud
        api.create = function (action,params) {
            return getUrl(action,'POST',params);
        };

        api.update = function (action,params) {
            return getUrl(action,'POST',params);
        };

        api.delete = function (action, params) {
            return getUrl(action, 'POST', params);
        };

        // login member
        api.loginMember = function(email, password){
            var deferred = $q.defer();
            var promise = deferred.promise;

            getUrl('/auth/member/login','POST', {
                username: email,
                password: password
            }).then(function(result){
                var res = result.data;
                var currentUser = localStorage.getItem('currentUser');
                
                if(res.status == 1){
                    localStorage.user_access = res.data.auth_key;
//                    if(currentUser==null || JSON.parse(currentUser).tanggal < DateFormat.getDate()){
                    if(currentUser==null){
                        localStorage.setItem("currentUser",JSON.stringify({tanggal:DateFormat.getDate(),data:res.data}));
                        currentUser = localStorage.getItem('currentUser');
                    }
                    $rootScope.currentUser = JSON.parse(currentUser).data;
                    deferred.resolve('Selamat Datang ' + email + '.');
                }else{
                    deferred.reject(res.message);
                }
            });
            promise.success = function(fn){
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn){
                promise.then(null, fn);
                return promise;
            }
            return promise;
        }

        api.deleteCache = function(){
            localStorage.clear();
        }

})
    .service('fileUpload', function ($http, $compile, $rootScope) {
        this.uploadFileToUrl = function(file, uploadUrl, option){
            var fd = new FormData();
            console.log(option);
            angular.forEach(option, function (i, val) {
                var name =  Object.keys(i)[0];
                fd.append(name, i[name]);
            });

            fd.append('file', file);
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function(s){
                    console.log(s);
                    var myEl = angular.element(document.querySelector( '#image-preview'));
                    var newEl = '<div class="list card" ng-click="xxx('+s.lastInsertId+')" >'+
                                    '<div class="item item-image">'+
                                        '<img src="'+ s.image_url+'">'+
                                    '</div>'+
                                '</div>';
                    var temp = $compile(newEl)($rootScope);
                    myEl.append(temp);
                })
                .error(function(e){
                    console.log(e);
                });
        }
    });
