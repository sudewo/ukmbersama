angular.module('app')
  .controller('menuCtrl', function($scope, $state, $ionicSideMenuDelegate, Api) {
    $scope.tasks = [
      {
        name: 'Orders',
        value: 'menu.orders'
      },
      {
        name: 'List Orders',
        value: 'menu.listorders'
      },
      {
        name: 'Reports',
        tree: [
          {
            name: 'Report Dashboard',
            value: 'menu.reports-dashboard',
          },
          {
            name: 'Report Per Order',
            value: 'menu.reports',
          },
          {
            name: 'Report Per Product',
            value: 'menu.reports-product',
          }
        ]
      },
      {
        name: 'Products',
        value: 'menu.productslist'
      },
      {
        name: 'Reset',
        value: 'menu.reset'
      },
      {
        name: 'Logout',
        value: 'menu.login'
      }
    ];



    $scope.$on('$ionTreeList:ItemClicked', function(event, item) {


      if(typeof(item.value) != 'undefined'){
        $state.go(item.value);
        $ionicSideMenuDelegate.toggleLeft();

        if(item.value == 'menu.login'){
            Api.deleteCache();
        }
      }

    });

  });
