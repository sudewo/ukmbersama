angular.module('app.routes', [])

    .config(function($stateProvider, $urlRouterProvider) {


        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider
            .state('menu.memberstore', {
                url: '/memberstore',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/memberstore.html',
                        controller : 'memberstoreCtrl'
                    }
                }
            })
            .state('menu.members', {
                url: '/members/:id',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/members.html',
                        controller : 'membersCtrl'
                    }
                }
            })
            .state('menu.barang', {
                url: '/barang/:id',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/barang.html',
                        controller : 'barangCtrl'
                    }
                }
            })
            .state('menu.category', {
                url: '/category/:id',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/category.html',
                        controller : 'categoryCtrl'
                    }
                }
            })
            //.state('menu.product', {
            //    url: '/product/:id',
            //    views: {
            //        'side-menu21': {
            //            templateUrl: 'templates/product.html',
            //            controller : 'productCtrl'
            //        }
            //    }
            //})
            .state('menu.orders', {
                url: '/orders/:id',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/orders/orders.html',
                        controller: 'ordersCtrl'
                    },

                }
            })
            .state('menu.listorders', {
                url: '/listorders/:id',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/listorders/listorders.html',
                        controller: 'listorderCtrl'
                    },

                }
            })

            .state('menu.reset', {
                url: '/reset',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/reset/reset.html',
                        controller: 'resetCtrl'
                    },

                }
            })
            .state('menu.meja', {
                url: '/meja',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/meja/meja.html',
                        controller: 'mejaCtrl'
                    },

                }
            })
            .state('menu.filter', {
                url: '/filter',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/filter.html'
                    }
                }
            })


            // START PRODUCT
            .state('menu.productslist', {
                url     : '/products/list',
                views   : {
                    'side-menu21' : {
                        templateUrl : 'templates/product/product.list.html',
                        controller  : 'product.list.controller'
                    }
                }
            })
            .state('menu.productadd', {
                url     : '/products/add',
                views   : {
                    'side-menu21' : {
                        templateUrl : 'templates/product/product.add.html',
                        controller  : 'product.add.controller'
                    }
                }
            })
            .state('menu.productdetail', {
                url     : '/products/detail/:idx',
                views   : {
                    'side-menu21' : {
                        templateUrl : 'templates/product/product.detail.html',
                        controller  : 'product.detail.controller'
                    }
                }
            })
            .state('menu.productupdate', {
                url     : '/products/update/:idx',
                views   : {
                    'side-menu21' : {
                        templateUrl : 'templates/product/product.form.html',
                        controller  : 'product.detail.controller'
                    }
                }
            })
            // END PRODUCT


            .state('menu', {
                url: '/side-menu21',
                abstract:true,
                templateUrl: 'templates/menu.html',
                controller: 'menuCtrl'
            })
            .state('tabsController.cameraTabDefaultPage', {
                url: '/page3',
                views: {
                    'tab1': {
                        templateUrl: 'templates/cameraTabDefaultPage.html'
                    }
                }
            })
            .state('cameraKeren', {
                url: '/page4',
                templateUrl: 'templates/cameraKeren.html'
            })
            .state('tabsController.cloudTabDefaultPage', {
                url: '/page5',
                views: {
                    'tab3': {
                        templateUrl: 'templates/cloudTabDefaultPage.html'
                    }
                }
            })
            .state('tabsController', {
                url: '/page2',
                abstract:true,
                templateUrl: 'templates/tabsController.html'
            })
            .state('menu.login', {
                url: '/login',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/login/login.html',
                        controller : 'loginCtrl'
                    }
                }
            })
            .state('menu.reports', {
                url: '/reports',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/reports/report.html',
                        controller : 'reportsCtrl'
                    }
                }
            })
            .state('menu.reports-detail', {
                url: '/reports-detail',
                params: {
                    member_id: '',
                    ds: '',
                    product_id: ''
                },
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/reports/report_detail.html',
                        controller : 'reportsDetailCtrl'
                    }
                }
            })
            .state('menu.reports-product', {
                url: '/reports-product',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/reports/report_product.html',
                        controller : 'reportsProductCtrl'
                    }
                }
            })
            .state('menu.reports-dashboard', {
                url: '/reports-dashboard',
                params: {
                    member_id: '',
                    ds: '',
                    product_id: ''
                },
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/reports/report_dashboard.html',
                        controller : 'reportsDashboardCtrl'
                    }
                }
            })
            .state('signup', {
                url: '/page9',
                templateUrl: 'templates/signup.html'
            })


        ;

        // if none of the above states are matched, use this as the fallback

        $urlRouterProvider.otherwise('/side-menu21/login');




    });
