angular.module('app.factory', [])

.factory('authInterceptor', function ($q, $window, $location, $rootScope, $log, $cookieStore) {
            return {
                request: function (config) {
                    config.headers = config.headers || {};
                    if (localStorage.getItem('user_access')) {
                        if (api_url.toLowerCase().indexOf($location.host()) >= 0) { // check if host like urlAPI
                            config.headers.Authorization = 'Bearer ' + localStorage.getItem('user_access');
                        }
                    }else {
                        $location.path('/login').replace();
                    }
                    $rootScope.loading = true;
                    return config;
                },
                requestError: function (rejection) {
                    $rootScope.loading = false;
                    $log.error('Request error:', rejection);
                    return $q.reject(rejection);
                },
                response: function (response) {
                    $rootScope.loading = false;
                    return response;
                },
                responseError: function (rejection) {
                    if (rejection.status === 401 || rejection.status === 403) {
                        $state.go('menu.login');
                    }
                    $rootScope.loading = false;
                    return $q.reject(rejection);
                }
            };
        })
.factory('DateFormat', [function () {
        var date = new Date;
        var getDay = function(){
            return ('0' + date.getDate()).slice(-2);
        };
        var getDate = function(){
            var date = new Date;
            return date.getFullYear()
                    + '-' + ('0' + (date.getMonth() + 1)).slice(-2)
                    + '-' + ('0' + date.getDate()).slice(-2);
        };
        var getDateTime = function(){
            var date = new Date;
            return date.getFullYear()
                    + '-' + ('0' + (date.getMonth() + 1)).slice(-2)
            return date.getFullYear()
                    + '-' + ('0' + (date.getMonth() + 1)).slice(-2)
                    + '-' + ('0' + date.getDate()).slice(-2);
        };
        var getDateTime = function(){
            return date.getFullYear()
                    + '-' + ('0' + (date.getMonth() + 1)).slice(-2)
                    + '-' + ('0' + date.getDate()).slice(-2)
                    + ' ' + ('0' + date.getHours()).slice(-2)
                    + ':' + ('0' + date.getMinutes()).slice(-2)
                    + ':' + ('0' + date.getSeconds()).slice(-2);
        };
        var getDateTimePlusDay = function(day){
            date.setDate(date.getDate() + day);
            return date.getFullYear()
                    + '-' + ('0' + (date.getMonth() + 1)).slice(-2)
                    + '-' + ('0' + date.getDate()).slice(-2)
                    + ' ' + ('0' + date.getHours()).slice(-2)
                    + ':' + ('0' + date.getMinutes()).slice(-2)
                    + ':' + ('0' + date.getSeconds()).slice(-2);
        };
        return {
            getDay              : getDay,
            getDate             : getDate,
            getDateTime         : getDateTime,
            getDateTimePlusDay  : getDateTimePlusDay
        };
    }])
    .factory('formatMoney', [function () {
        var readNumber = function(number, decimals, dec_point, thousands_sep){
            number = (number + '')
                .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function(n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + (Math.round(n * k) / k)
                            .toFixed(prec);
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
                .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
                    .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
                    .join('0');
            }

            return s.join(dec);
        }

        return {
            readNumber: readNumber
        };
    }])
    .factory('getKursi', [function () {
        var getAllKursi = function(){
            var kursi = [];
            Object.keys(localStorage).forEach(function(i, j){
                kursi.push(i.split('#')[0]);
            });
            return kursi;
        }
        var getAllData = function(){
            var kursi = [];
            Object.keys(localStorage).forEach(function(i, j){
                kursi.push(i);
            });
            return kursi;
        }
        var getArrKursi = function(id){
            var kursi = [];
            if(id != undefined){
                Object.keys(localStorage).forEach(function(i, j){
                    for (var k=0; k<id.length; k++){
                        if(i.split('#')[0] == id[k])
                            kursi.push(i.split('#')[0]);
                    }
                });
            }
            return kursi;
        }
        var getArrData = function(id){
            var kursi = [];
            if(id != undefined){
                Object.keys(localStorage).forEach(function(i, j){
                    for (var k=0; k<id.length; k++){
                        if(i.split('#')[0] == id[k])
                            kursi.push(i);
                    }
                });
            }
            return kursi;
        }
        return {
            getAllKursi: getAllKursi,
            getAllData: getAllData,
            getArrKursi: getArrKursi,
            getArrData: getArrData
        };

    }]);
