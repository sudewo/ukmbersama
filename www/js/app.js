// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic', 'app.routes', 'app.services', 'app.directives', 'app.factory', 'ui.utils.masks', 'chart.js', 'ionic-datepicker','angularMoment','ion-tree-list', 'ngCordova'])
//.run(function($ionicPlatform) {
//  $ionicPlatform.ready(function() {
//    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
//    // for form inputs)
//    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
//      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
//      cordova.plugins.Keyboard.disableScroll(true);
//    }
//    if (window.StatusBar) {
//      // org.apache.cordova.statusbar required
//      StatusBar.styleDefault();
//    }
//
//  });
//})
//.run(function(amMoment) {
//    amMoment.changeLocale('id');
//    moment.locale('id');
//})
.run(function($ionicPlatform, $rootScope, DateFormat, Api, $location, $state, amMoment, $log, $timeout) { // buat konstanta
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
          // org.apache.cordova.statusbar required
          StatusBar.styleDefault();
        }

      });
    
    if (!localStorage.getItem('user_access')) { // if not login
        $location.path('/login').replace();
    }else{ // if login
      if(localStorage.getItem('/statusukm/list')){
          var statusUkm = localStorage.getItem('/statusukm/list');
          $rootScope.status = JSON.parse(statusUkm).data;
      }else{
          Api.getAll('/statusukm/list')
          .then(function (result) {
              localStorage.setItem("/statusukm/list",JSON.stringify({tanggal:DateFormat.getDate(),data:result.data}));
              $rootScope.status = JSON.parse(localStorage.getItem('/statusukm/list')).data;
          });
      };
      
      if(localStorage.getItem('currentUser')){
          var currentUser = JSON.parse(localStorage.getItem('currentUser')).data;
          $rootScope.currentUser = currentUser;
          
          if(currentUser.member_store_status == '0'){ // check if status store is not active
              console.log('Silahkan Lengkapi Data');
          };
      }
    }
    
    $rootScope.$on("$locationChangeStart", function(current, next){
//        $log.info(next); 
        if(localStorage.getItem('currentUser')){
            var currentUser = JSON.parse(localStorage.getItem('currentUser')).data;
            
            if(currentUser.member_store_status == '0'){ // check if status store is not active
              $timeout(function () { // run after digest complete
                angular.element(document.querySelectorAll('.modalstoreform')).triggerHandler('click');
              });
            };   
        }
    });
                
    $rootScope.$on('$viewContentLoaded', function() {
        
    });
    
    document.addEventListener("deviceReady", function () {
        
    }, false);
    
    amMoment.changeLocale('id');
    moment.locale('id');
});
