angular.module('app.directives', [])

    .directive('blankDirective', [function(){

    }])
    .directive('ngDetail',[function (){
        return {
            restrict: 'E',
//            scope: {
//                source: '=',
//                id: '=',
//            },
//            controller: function ($scope, $timeout) {
//                $timeout(function(){
//                    $scope.tes = $scope.source;
//                },100);
//
//            },
//            link: function ($scope) {
//                console.log($scope)
//            },
            templateUrl: '../templates/detailview.html'
//            template: '<div>{{source}}</div>'
        };
    }])
    .directive('showErrors', [function(){
        return {
            restrict: 'A',
            require:'^form',
            link: function (scope,el,attrs,formCtrl){
                // find the text box element, which has the 'name attribute
                var inputEl = el[0].querySelector("[name]");

                // convert the native text box element to an angular element
                var inputNgEl = angular.element(inputEl);

                // get the name on the text box we know the property to check
                // on the form controller
                var inputName = inputNgEl.attr('name');

                var helpText = angular.element(el[0].querySelector(".help-block"));

                // only aplly the has-error class after the user leaves the text box
                inputNgElbind('blur',function (){
                    el.toggleClass('has-error', formCtrl[inputName].$invalid);
                    helpText.toggleClass('hide', formCtrl[inputName].$valid);
                });
            }
        };
    }])
    .directive( 'goClick', function ( $location ) {
        return function ( scope, element, attrs ) {
            var path;

            attrs.$observe( 'goClick', function (val) {
                path = val;
            });

            element.bind( 'click', function () {
                scope.$apply( function () {
                    $location.path( path );
                });
            });
        };
    })

    .directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function () {
                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }])
    .directive('modalStoreForm', ['$ionicModal', function($ionicModal){
            return {
                transclude: true,
                restrict: 'EA',
                template: '<a ng-show="{{showBtn}}" class="{{class}}" ng-click="openmodalstoreform()" ng-transclude></a>',
                scope: {
                    showBtn: "@",
                    class:  "@"
                },
                link: function ($scope, $element, $attrs) {
                    $ionicModal.fromTemplateUrl('templates/daftarbaru/_modalstoreform.html', {
                        scope: $scope,
                        animation: 'slide-in-up'
                    }).then(function(modal) {
                        $scope.modalstoreform = modal;

                    });

                    $scope.openmodalstoreform = function(){
                        $scope.modalstoreform.show();
                    };

                    $scope.reset = function(){
                        $scope.data = {};
//                        $scope.data.name = '';
//                        $scope.data.email = '';
//                        $scope.data.password = '';
                        $scope.submitted = false;
                        $scope.modalstoreform.hide();
                    };
                }
            };
    }])
