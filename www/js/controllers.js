// //var myApp =
//     //     $scope.createOrder = function (order) {
//     //         var newOrderData =
//     //             {
//     //                 'table': order.table,
//     //                 'order': [
//     //
//     //                     {
//     //                         'menu': 'ayam bakar',
//     //                         'quantity': 1
//     //                     },
//     //                     {
//     //                         'menu': 'mie ayam',
//     //                         'quantity': 1
//     //                     },
//     //                     {
//     //                         'menu': 'es teh manis',
//     //                         'quantity': 2
//     //                     }
//     //                 ]
//     //             };
//     //         localStorage.setItem("order",JSON.stringify(newOrderData));
//     //         $scope.orders.push(newOrderData);
//     //         //$scope.closeModal();
//     //     }
//     // })
// angular.module('app')
//     .controller('productsCtrl', function($http,$scope, $ionicModal, $ionicPopup, $timeout,$stateParams,$window,$state, getKursi, Api, $rootScope) {
//
//         $scope.id = ($stateParams.id!='' && $stateParams.id !=undefined ? $stateParams.id : '');
//         $scope.show_order = [];
//         $scope.orders = [];
//
//         function initCreateForm() {
//             $scope.newObject = {
//                 order_id:$scope.id,
//                 member_id: $rootScope.currentUser.member_id,
//                 member_store_id: $rootScope.currentUser.member_store_id,
//                 order_transaction_number:"",
//             };
//         }
//         initCreateForm();
//
//         function order_item(index, qty) {
//             return {
//                 product_item_id:index.product_item_id,
//                 product_category:index.product_category_id,
//                 product_item_stock:index.product_item_stock,
//                 order_qty:(qty==undefined?1:qty),
//                 product_item_price:index.product_item_price,
//                 product_title:index.product_title,
//                 product_item_name:index.product_item_name,
//                 sub_harga:1*index.product_item_price,
//             }
//         }
//
//         function splice(index) {
//             $scope.show_order.splice($scope.show_order.indexOf(index.product_item_id), 1);
//             $scope.orders.forEach(function(i, j){
//                 if(i.product_item_id == index.product_item_id){
//                     $scope.orders.splice(j, 1);
//                 }
//             });
//         }
//
//         function push(index,i) {
//             $scope.show_order.push(index.product_item_id);
//             $scope.orders.push(order_item(index, (i==undefined? undefined : i.order_qty)));
//         }
//
//         function list() {
//             Api.getAll('/product/list',{
//                 member_id: $rootScope.currentUser.member_id,
//                 member_store_id: $rootScope.currentUser.member_store_id,
//             })
//             .then(function (result) {
//                 $scope.data = result.data.rows;
// //                console.log($scope.data)
//             });
//         }
//
//         function list_order() {
//             Api.getAll('/orderdetail/list', {
//                 member_id: $rootScope.currentUser.member_id,
//                 member_store_id: $rootScope.currentUser.member_store_id,
//                 order_id: $scope.id,
//             }).then(function (result) {
//                 $scope.list_order = result.data.rows;
//             });
//         }
//         list();
//
//         function cek_list_order () {
//             Api.getAll('/orders/list', {
//                 member_id: $rootScope.currentUser.member_id,
//                 member_store_id: $rootScope.currentUser.member_store_id,
//             }).then(function (result) {
//                 $scope.data_list_order = result.data.rows;
//             });
//         }
//
//         function getCategory () {
//             Api.getAll('/productcategories/list', {
//                 member_id: $rootScope.currentUser.member_id,
//                 member_store_id: $rootScope.currentUser.member_store_id,
//             }).then(function (result) {
//                 $scope.data_category = result.data.rows;
//             });
//         }
//         getCategory();
//
//         if($scope.id!='') {
//             list_order();
//             $timeout(function(){
//                 $scope.list_order.forEach(function(i, j){
//                     $scope.data.forEach(function(k, l){
//                         if(k.product_item_id == i.product_item_id) {
//                             push(k,i);
//                         }
//                     });
//                 });
//                 $scope.newObject.order_transaction_number = $scope.list_order[0].order_resi;
//             },100);
//         }else{
//
//         }
//
//         $scope.checked = function(index,status) {
//             if($scope.show_order.indexOf(index.product_item_id) !== -1){
//                 if(parseInt(status) == 1) {
//                     var confirmPopup = $ionicPopup.confirm({
//                         title: 'Delete',
//                         template: 'Yakin mau dihapus?'
//                     });
//                     confirmPopup.then(function (res) {
//                         if (res) {
//                             splice(index);
//                         }
//                     });
//                 } else {
//                     splice(index);
//                 }
//
//             }else{
//                 var order_lama = false;
//                 if($scope.id!='') {
//                     $scope.list_order.forEach(function(i, j){
//                         if(i.product_item_id==index.product_item_id) {
//                             push(index,i);
//                             order_lama = true;
//                         }
//                     });
//                 }
//                 if(!order_lama){
//                     push(index);
//                 }
//             }
//         };
//
//         $scope.plus = function (item) {
//             $scope.orders.forEach(function(i, j){
//                 if(i.product_item_id == item.product_item_id){
//                     if(parseInt(i.order_qty) >= parseInt(item.product_item_stock)) {
//                         alert('stock hanya '+item.product_item_stock);
//                     } else {
//                         i.order_qty++;
//                         i.sub_harga = i.order_qty * item.product_item_price;
//                     }
//                 }
//             });
//         };
//
//         $scope.min = function (item) {
//             $scope.orders.forEach(function(i, j){
//                 if(i.product_item_id == item.product_item_id){
//                     if(parseInt(i.order_qty) <= 1) {
//                         alert('minimal order 1');
//                     } else {
//                         i.order_qty--;
//                         i.sub_harga = i.order_qty * item.product_item_price;
//                     }
//                 }
//             });
//         }
//
//
//         $ionicModal.fromTemplateUrl('order.html', {
//             scope: $scope,
//             animation: 'slide-in-up'
//         }).then(function(modal) {
//             $scope.modalorder = modal;
//
//         });
//
//         $scope.kategorichange = function(item){
//             $scope.search = item.product_category_id;
//         }
//
//         $scope.sortproduct = [
//             {text: 'termurah',value: 'price'},
//             {text: 'hot item',value: '-sticky'},
//         ];
//
//         $ionicModal.fromTemplateUrl('my-modal.html', {
//             scope: $scope,
//             animation: 'slide-in-up'
//         }).then(function(modal) {
//             $scope.modal = modal;
//
//         });
//
//         $scope.getTotal = function(){
//             var total = 0;
//             if($scope.orders.length) {
//                 $scope.orders.forEach(function(i, j){
//                     total += i.sub_harga;
//                 });
//             }
//             return total;
//         }
//
//         $scope.openModal = function() {
//             $scope.modal.show();
//         };
//
//         $scope.cancelModal = function() {
//             $scope.modal.hide();
//             $scope.search = '';
//         };
//
//         $scope.applyModal = function() {
// //    console.log($scope.choice);
// //      return false;
//             $scope.modal.hide();
//         };
//
//         $ionicModal.fromTemplateUrl('order.html', {
//             scope: $scope,
//             animation: 'slide-in-up'
//         }).then(function(modal) {
//             $scope.modalorder = modal;
//
//         });
//
//         $scope.openOrder = function()
//         {
//             if($scope.orders.length) {
//                 $scope.modalorder.show();
//             } else {
//                 alert('Belum ada orderan!');
//             }
//         };
//
//         $scope.orderNow = function() {
//             if($scope.orders.length) {
//                 $scope.order_member.show();
//             } else {
//                 alert('Belum ada orderan!');
//             }
//         };
//
//         $ionicModal.fromTemplateUrl('order-member.html', {
//             scope: $scope,
//             animation: 'slide-in-up'
//         }).then(function (modal) {
//             $scope.order_member = modal;
//         });
//         $scope.closeOrderMember = function () {
//             $scope.order_member.hide();
//         };
//
//         $scope.saveOrderMember = function () {
//             cek_list_order();
//
//             $timeout(function (){
//                 // cek kursi sudah ada atau belum
//                 var kursi_ada = false;
//                 $scope.data_list_order.forEach(function(i, j){
//                     if(i.order_transaction_number==$scope.newObject.order_transaction_number && i.order_status==0 && $scope.id==''){
//                         kursi_ada = true;
//                     }
//                 });
//
//                 if(kursi_ada){
//                     alert('Kursi sudah ada');
//                 }else if($scope.newObject.order_transaction_number!=''){
//                     $scope.newObject.orders = JSON.stringify($scope.orders);
//                     Api.create('/orders'+($scope.id!=''?'/update':'/add'), $scope.newObject
//                     ).then(function (result) {
//                         if(result.data!=0){
//                             $scope.order_member.hide();
//                             $scope.modalorder.hide();
//                             $scope.orders = [];
//                             $scope.show_order = [];
//                             initCreateForm();
// //                            $state.transitionTo("menu.products",null,{reload: true,inherit: false,notify: true});
// //                            $state.transitionTo("menu.products");
// //                            $state.transitionTo("menu.listorders");
//                         } else {
//                             console.log('ada error');
//                         }
//                     });
//                 } else {
//                     alert('Kursi harus di isi...');
//                 }
//             },100);
//         };
//
//         $scope.cancelModalOrder = function() {
//             $scope.modalorder.hide();
//             $scope.search = '';
//         };
//
//         $scope.applyModal = function() {
// //    console.log($scope.choice);
// //      return false;
//             $scope.modalorder.hide();
//         };
//
//         // Triggered on a button click, or some other target
//         $scope.showPopup = function() {
//             $scope.data = {}
//
//             // An elaborate, custom popup
//             var myPopup = $ionicPopup.show({
//                 templateUrl: 'popup-template.html',
//                 title: 'Sort Products',
//                 scope: $scope,
//                 buttons: [
//                     { text: 'Cancel' },
//                     {
//                         text: '<b>Save</b>',
//                         type: 'button-positive',
//                         onTap: function(e) {
//                             console.log($scope.data)
//                             return $scope.data;
//                         }
//                     },
//                 ]
//             });
//
//             myPopup.then(function(res) {
//                 console.log('Tapped!', res);
//                 if(res !== undefined)
//                     $scope.sort = res.sort;
//             });
//
// //   $timeout(function() {
// //      myPopup.close(); //close the popup after 3 seconds for some reason
// //   }, 3000);
//         };
//
//     })
//
//
//     .controller('filterCtrl', function($scope) {
//
//     })
//
//     .controller('sortCtrl', function($scope){
//         console.log($scope);
//     })
//
//
//     .controller('cameraTabDefaultPageCtrl', function($scope) {
//
//     })
//
//     .controller('cameraKerenCtrl', function($scope) {
//
//     })
//
//     .controller('cloudTabDefaultPageCtrl', function($scope) {
//
//     })
//
//     .controller('loginCtrl', function($scope) {
//
//     })
//
//     .controller('signupCtrl', function($scope) {
//
//     })
// //    $scope.list = []
//     .controller('listorderCtrl', function($scope,$ionicPopup,$window,$state, formatMoney, $rootScope, Api, $stateParams,$timeout,$ionicModal) {
//         $scope.id = ($stateParams.id!='' && $stateParams.id !=undefined ? $stateParams.id : '');
//         function viewDefault () {
//             $scope.view = {
//                 title: ($scope.id==''? 'List Order' : 'Order Detail'),
//                 create: false,
//                 update: false,
//                 detail: ($scope.id==''? false : true),
//             };
//         }
//         viewDefault();
//
//         function list () {
//             Api.getAll('/orders/list', {
//                 member_id: $rootScope.currentUser.member_id,
//                 member_store_id: $rootScope.currentUser.member_store_id,
//             }).then(function (result) {
//                 $scope.data = result.data.rows;
//             });
//         }
//
//         function listdetail () {
//             Api.getAll('/orders/listdetail', {
//                 member_id: $rootScope.currentUser.member_id,
//                 member_store_id: $rootScope.currentUser.member_store_id,
//                 order_id: $scope.id,
//             }).then(function (result) {
//                 $scope.data_detail = result.data.rows;
//             });
//         }
//
//         function total_order(){
//             $timeout(function(){
//                 var total = 0;
//                 if($scope.data_detail.length) {
//                     $scope.data_detail.forEach(function(i, j){
//                         total += parseInt(i.order_price);
//                     });
//                 }
//                 $scope.total_order = total;
//             },100);
//         };
//
//         function status_page() {
//             if($scope.id=='') {
//                 list();
//             } else {
//                 listdetail();
//                 total_order();
//             }
//         }
//         status_page();
//
//
//         // klik ke detail
//         $scope.urlTo = function (data){
// //            console.log(data)
//             $state.transitionTo("menu.listorders",{'id':data.order_id});
//         };
//
//
//         $scope.deleteModal = function (key) {
//             var confirmPopup = $ionicPopup.confirm({
//                 title: 'Delete',
//                 template: 'Yakin mau dihapus?'
//             });
//             confirmPopup.then(function (res) {
//                 if (res) {
//                     Api.delete('/orders/delete', key
//                     ).then(function (result) {
//                         if(result.data==1){
//                             status_page();
//                         } else {
//                             console.log('ada error');
//                         }
//                     });
//                 }
//             });
//         };
//
//         $scope.updateModal = function (key) {
//             $state.transitionTo("menu.products",{'id':key.order_id});
//         };
//
// //        $ionicModal.fromTemplateUrl('modal-bayar.html', {
// //            scope: $scope,
// //            animation: 'slide-in-up'
// //        }).then(function (modal) {
// //            $scope.modal_bayar = modal;
// //        });
// //        $scope.openModalBayar = function () {
// //            $scope.modal_bayar.show();
// //        };
// //        $scope.closeModalBayar = function () {
// //            $scope.modal_bayar.hide();
// //        };
//
//
//         $scope.field = {
//             id:$scope.id,
//             order_status:1
//         };
//         $scope.kembalian = function (){
//             $timeout(function (){
//                 $scope.field.kembali = $scope.field.bayar - $scope.total_order;
//             },100);
//         };
//
//         $scope.openModalBayar = function() {
//             $scope.field.kembali = 0 - $scope.total_order;
//             // An elaborate, custom popup
//             var myPopup = $ionicPopup.show({
//               template: 'Pembayaran <input type="text" style="text-align: right;" ng-keydown="kembalian()" ng-model="field.bayar" ui-number-mask="-3"><br>'+
//                         'Kembalian <h1 style="float: right;">{{field.kembali | number:2}}</h1>',
//               title: 'Pembayaran',
//               subTitle: 'Please use normal things',
//               scope: $scope,
//               buttons: [
//                 { text: 'Cancel' },
//                 {
//                   text: '<b>Save</b>',
//                   type: 'button-positive',
//                   attr: 'ng-disabled=false',
//                   onTap: function(e) {
//                     if ($scope.field.kembali<0) {
//                       e.preventDefault();
//                     } else {
//                       return $scope.field;
//                     }
//                   }
//                 },
//               ]
//             });
//             myPopup.then(function(res) {
//                 if(res!=undefined) {
//                     Api.create('/orders/update', $scope.field
//                     ).then(function (result) {
//                         if(result.data!=0){
//                             $scope.field = {};
//                             status_page();
// //                            $state.transitionTo("menu.products",null,{reload: true,inherit: false,notify: true});
// //                            $state.transitionTo("menu.products");
// //                            $state.transitionTo("menu.listorders");
//                         } else {
//                             console.log('ada error');
//                         }
//                     });
//
//                 } else {
// //
//                 }
//             });
//
//         }
//
//
//
//     })
//
//     .filter('rupiah', function () {
//         return	function	toRp(angka) {
//             var rev = parseInt(angka, 10).toString().split('').reverse().join('');
//             var rev2 = '';
//             for (var i = 0; i < rev.length; i++) {
//                 rev2 += rev[i];
//                 if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
//                     rev2 += '.';
//                 }
//             }
//             return	'Rp.	' + rev2.split('').reverse().join('') + ',00';
//         }
//     })
//     .filter('kursi', function() {
//         return function(input, search) {
//             if (!input) return input;
//             if (!search) return input;
//             var result = {};
//             angular.forEach(input, function(value, key) {
//                 if (key == search) {
//                     result[key] = value;
//                 }
//             });
//             return result;
//         }
//
// //        return function(input, search) {
// //          if (!input) return input;
// //          if (!search) return input;
// //          var expected = ('' + search).toLowerCase();
// //          var result = {};
// //          angular.forEach(input, function(value, key) {
// //            var actual = ('' + value).toLowerCase();
// //            if (actual.indexOf(expected) !== -1) {
// //              result[key] = value;
// //            }
// //          });
// //          return result;
// //        }
//     })
//     .controller('membersCtrl', function($scope, $ionicModal, Api, DateFormat, $state, $stateParams, $ionicPopup) {
//         $scope.view = {
//             title: 'Member',
//
//         };
//         //get list
//         function list() {
//             Api.getAll('/members/list', {product_id:1, product_active:1})
//             .then(function (result) {
//                 $scope.data = result.data.rows;
//                 detailId();
//             });
//         }
//
//         function detailId (){
//             $scope.showDetail = false;
//             $scope.dataDetail = '';
//             if($stateParams.id!='') {
//                 $scope.id = $stateParams.id;
//                 $scope.showDetail = true;
//                 angular.forEach($scope.data, function (value, key){
//                     if(value.member_id==$stateParams.id) {
//                         $scope.dataDetail = value;
//                     }
//                 });
//             }
//         };
//
//         function initCreateForm() {
//             $scope.newObject = {
//                 name: "",
//                 email: "",
//                 active: ""
//             };
//         }
//
//         // klik ke detail
//         $scope.urlTo = function (data){
//             $state.transitionTo("menu.members",{'id':data.member_id});
//         };
//
//         $scope.createMemberModal = function () {
//             $scope.hidePassword = false;
//             $scope.modalMember.show();
//         };
//
//         $scope.updateMemberModal = function (data) {
//             $scope.modalMember.show();
//             $scope.hidePassword = true;
//             $scope.newObject = angular.copy(data);
//         };
//
//         $scope.deleteMember = function (data) {
//             var confirmPopup = $ionicPopup.confirm({
//                     title: 'Delete Member',
//                     template: 'Yakin mau dihapus?'
//                 });
//                 confirmPopup.then(function (res) {
//                     if (res) {
//                         console.log(data.member_id);
//                         Api.create('/auth/member/delete', data)
//                         .then(function (result) {
//                             console.log(result); //Object {data: "3", status: 200, config: Object, statusText: "OK"}
//                             if(result.status==200 &&result.data!="") {
//                                 $scope.modalMember.hide();
//                                 list();
//                             }
//                         });
//                     }
//                 });
//
//         };
//
//         $scope.sendMember = function (object) {
// //            delete object.member_id;
//             Api.create('/auth/member'+(!$scope.hidePassword?'/register':'/update'), object)
//             .then(function (result) {
//                 console.log(result); //Object {data: "3", status: 200, config: Object, statusText: "OK"}
//                 if(result.status==200 &&result.data!="") {
//                     $scope.modalMember.hide();
//                     list();
//                     initCreateForm();
//                 }
//             });
//
//         };
//
//
//
//
//
//         $ionicModal.fromTemplateUrl('addMember.html', {
//             scope: $scope,
//             animation: 'slide-in-up'
//         }).then(function (modal) {
//             $scope.modalMember = modal;
//         });
//
//         $scope.closeMemberModal = function () {
//             $scope.modalMember.hide();
//         };
//
//         initCreateForm();
//         list();
//
//     })
//     .controller('memberstoreCtrl', function($scope, $ionicModal, Api, DateFormat) {
//
//         function list() {
//             Api.getAll('/memberstore/list')
//             .then(function (result) {
//                 $scope.data = result.data.rows;
//                 console.log($scope.data)
//             });
//         }
//
//         $scope.update = function (object) {
// //            updated_at: "0000-00-00 00:00:00",
//         }
//         $scope.createMember = function (object) {
// //            console.log(object)
// //            Api.create('/auth/member/register', object)
// //            .then(function (result) {
// //                console.log(result)
//                 $scope.closeMemberModal();
// //                $scope.data = result.data.rows;
// //            });
//         }
//
//         function initCreateForm() {
//             $scope.newObject = {
//                 name: "",
//                 email: "",
//                 active: "",
// //                created_at: "",
// //                member_store_name: "",
// //                member_store_logo: "",
// //                member_store_address: "",
// //                member_store_phone: "",
// //                member_store_lat: "",
// //                member_store_lng: "",
// //                member_store_type: "",
// //                member_store_status: "",
// //                province_id: "",
// //                city_id: ""
//             };
//         }
//
//
//
//         initCreateForm();
//         list();
//
//         $ionicModal.fromTemplateUrl('addMember.html', {
//             scope: $scope,
//             animation: 'slide-in-up'
//         }).then(function (modal) {
//             $scope.modalMember = modal;
//         });
//         $scope.openMemberModal = function () {
//             initCreateForm();
//             $scope.modalMember.show();
//         };
//         $scope.closeMemberModal = function () {
//             $scope.modalMember.hide();
//         };
//
//
//
//
//
//         $ionicModal.fromTemplateUrl('addStore.html', {
//             scope: $scope,
//             animation: 'slide-in-up'
//         }).then(function (modal) {
//             $scope.modalStore = modal;
//         });
//         $scope.openStoreModal = function () {
//             initCreateForm();
//             $scope.modalStore.show();
//         };
//         $scope.closeStoreModal = function () {
//             $scope.modalStore.hide();
//         };
//     })
//     .controller('productCtrl', function($scope, $ionicModal, Api, DateFormat, $stateParams, $state, $rootScope) {
//         $scope.id = ($stateParams.id!='' && $stateParams.id !=undefined ? $stateParams.id : '');
//         function viewDefault () {
//             $scope.view = {
//                 title: 'Product',
//                 create: false,
//                 update: false,
//                 detail: ($scope.id==''? false : true),
//                 dataDetail: '',
//                 product_category_title: '',
//             };
//         };
//         //get list
//         function list() {
//             Api.getAll('/product/list', {
//                 member_id: $rootScope.currentUser.member_id,
//                 member_store_id: $rootScope.currentUser.member_store_id,
//             }).then(function (result) {
//                 $scope.data = result.data.rows;
//                 detailId();
//             });
//         }
//
//         function getCategory () {
//             Api.getAll('/productcategories/list', {
//                 member_id: $rootScope.currentUser.member_id,
//                 member_store_id: $rootScope.currentUser.member_store_id,
//             }).then(function (result) {
//                 $scope.data_category = result.data.rows;
//             });
//         }
//
//         function detailId (){
//             if($scope.id!='') {
//                 $scope.view.detail = true;
//                 angular.forEach($scope.data, function (value, key){
//                     if(value.member_id==$scope.id) {
//                         $scope.dataDetail = value;
//                     }
//                 });
//             }
//         };
//
//         function initCreateForm() {
//             $scope.newObject = {
//                 member_id: $rootScope.currentUser.member_id,
//                 member_store_id: $rootScope.currentUser.member_store_id,
//                 product_category_id : "",
//                 product_active: "",
//                 product_title: "",
//             };
//         }
//
//         // klik ke detail
//         $scope.urlTo = function (data){
//             $state.transitionTo("menu.product",{'id':data.member_id});
//         };
//
//         $scope.createModal = function () {
//             $scope.view.create = true;
//             $scope.modal.show();
//             getCategory();
//         };
//
//         $scope.updateModal = function (data) {
//             $scope.modal.show();
//             $scope.view.update = true;
//             $scope.newObject = angular.copy(data);
//             getCategory();
//         };
//
//         $scope.deleteModal = function (data) {
//             var confirmPopup = $ionicPopup.confirm({
//                     title: 'Delete '+$scope.view.title,
//                     template: 'Yakin mau dihapus?'
//                 });
//                 confirmPopup.then(function (res) {
//                     if (res) {
//                         Api.create('/products/delete', data)
//                         .then(function (result) {
//                             console.log(result); //Object {data: "3", status: 200, config: Object, statusText: "OK"}
//                             if(result.status==200 &&result.data!="") {
//                                 $scope.modal.hide();
//                                 list();
//                             }
//                         });
//                     }
//                 });
//
//         };
//
//         $scope.send = function (object) {
//
//
//             console.log(object);
//             return false;
//
//             Api.create('/product'+($scope.view.create?'/add':'/update'), object)
//             .then(function (result) {
//                 console.log(result); //Object {data: "3", status: 200, config: Object, statusText: "OK"}
//                 if(result.status==200 &&result.data!="") {
//                     $scope.modal.hide();
//                     list();
//                     initCreateForm();
//                     viewDefault();
//                 }
//             });
//
//         };
//
//         $ionicModal.fromTemplateUrl('add.html', {
//             scope: $scope,
//             animation: 'slide-in-up'
//         }).then(function (modal) {
//             $scope.modal = modal;
//         });
//
//         $scope.closeModal = function () {
//             $scope.modal.hide();
//             initCreateForm();
//             viewDefault();
//         };
//
//
//
//
//
//         $ionicModal.fromTemplateUrl('search.html', {
//             scope: $scope,
//             animation: 'slide-in-up'
//         }).then(function (modal) {
//             $scope.modalSearch = modal;
//         });
//
//         $scope.searchModal = function () {
//             $scope.modalSearch.show();
//         };
//
//         $scope.closeSearch = function () {
//             $scope.modalSearch.hide();
//         };
//
//
//         $scope.searchCategory = function() {
//             console.log('dsa');
//         }
//
//         $scope.selectSearch = function (data) {
//             console.log(data)
//             $scope.view.product_category_title = data.product_category_title;
//             $scope.newObject.product_category_id = data.product_category_id;
//             $scope.modalSearch.hide();
//         }
//
//
//
//         initCreateForm();
//         list();
//         viewDefault();
//
//
//
//
//     })
//     .controller('categoryCtrl', function($scope, $ionicModal, Api, DateFormat, $state, $stateParams, $rootScope) {
//
//         $scope.id = ($stateParams.id!='' && $stateParams.id !=undefined ? $stateParams.id : '');
//         function viewDefault () {
//             $scope.view = {
//                 title: 'Category',
//                 create: false,
//                 update: false,
//                 detail: ($scope.id==''? false : true),
//             };
//         }
//         //get list
//         function list() {
//             Api.getAll('/productcategories/list')
//             .then(function (result) {
//                 $scope.data = result.data.rows;
//                 detailId();
//             });
//         }
//
//         function detailId (){
//             $scope.showDetail = false;
//             $scope.dataDetail = '';
//             if($scope.id!='') {
//                 $scope.showDetail = true;
//                 angular.forEach($scope.data, function (value, key){
//                     if(value.product_category_id==$scope.id) {
//                         $scope.dataDetail = value;
//                     }
//                 });
//             }
//         };
//
//         function initCreateForm() {
//             $scope.newObject = {
//                 "member_id": $rootScope.currentUser.member_id,
//                 "member_store_id": $rootScope.currentUser.member_store_id,
//                 "product_category_title": "",
//                 "product_category_title_slug": "",
//                 "product_category_desc": "",
//                 "product_category_image": "",
//                 "product_category_parent": "",
//                 "product_category_active": 0,
//                 "homepage": ""
//             };
//         }
//
//         // klik ke detail
//         $scope.urlTo = function (data){
//             $state.transitionTo("menu.category",{'id':data.product_category_id});
//         };
//
//         $scope.createCategoryModal = function () {
//             $scope.view.create = true;
//             $scope.modalCategory.show();
//         };
//
//         $scope.updateCategoryModal = function (data) {
//             $scope.view.update = true;
//             $scope.modalCategory.show();
//             $scope.newObject = angular.copy(data);
//         };
//
//         $scope.closeCategory = function () {
//             $scope.modalCategory.hide();
//             initCreateForm();
//             viewDefault();
//         };
//
//         $scope.sendMember = function (object) {
//             Api.create('/productcategories'+($scope.view.create?'/add':'/update'), object)
//             .then(function (result) {
//                 if(result.status==200 &&result.data!="") {
//                     $scope.modalCategory.hide();
//                     list();
//                     initCreateForm();
//                     viewDefault();
//                 }
//             });
//
//         };
//
//         $scope.deleteMember = function (data) {
//             var confirmPopup = $ionicPopup.confirm({
//                     title: 'Delete '+$scope.view.title,
//                     template: 'Yakin mau dihapus?'
//                 });
//                 confirmPopup.then(function (res) {
//                     if (res) {
//                         Api.create('productcategories/delete', data)
//                         .then(function (result) {
//                             console.log(result); //Object {data: "3", status: 200, config: Object, statusText: "OK"}
//                             if(result.status==200 &&result.data!="") {
//                                 $scope.modalCategory.hide();
//                                 list();
//                             }
//                         });
//                     }
//                 });
//
//         };
//
//
//         $ionicModal.fromTemplateUrl('addCategory.html', {
//             scope: $scope,
//             animation: 'slide-in-up'
//         }).then(function (modal) {
//             $scope.modalCategory = modal;
//         });
//
//         initCreateForm();
//         list();
//         viewDefault();
//     })
//     .controller('barangCtrl', function($scope, $ionicModal, Api, DateFormat, $state, $stateParams, $rootScope) {
//         function viewDefault () {
//             $scope.view = {
//                 title: 'barang',
//                 create: false,
//                 update: false,
//             };
//         }
//         viewDefault();
//
//         //get list
//         function list() {
//             Api.getAll('/'+$scope.view.title+'/list')
//             .then(function (result) {
//                 $scope.data = result.data.rows;
//                 detailId();
//             });
//         }
//         list();
//
//         function detailId (){
//             $scope.showDetail = false;
//             $scope.dataDetail = '';
//             if($stateParams.id!='') {
//                 $scope.id = $stateParams.id;
//                 $scope.showDetail = true;
//                 angular.forEach($scope.data, function (value, key){
//                     if(value.product_category_id==$stateParams.id) {
//                         $scope.dataDetail = value;
//                     }
//                 });
//             }
//         };
//
//         function initCreateForm() {
//             $scope.newObject = {
//                 "member_id": $rootScope.currentUser.member_id,
//                 "member_store_id": $rootScope.currentUser.member_store_id,
//                 "product_category_title": "",
//                 "product_category_title_slug": "",
//                 "product_category_desc": "",
//                 "product_category_image": "",
//                 "product_category_parent": "",
//                 "product_category_active": 0,
//                 "homepage": ""
//             };
//         }
//         initCreateForm();
//
//         // klik ke detail
//         $scope.urlTo = function (data){
//             $state.transitionTo("menu."+$scope.view.title,{'id':data.product_category_id});
//         };
//
//         $scope.createCategoryModal = function () {
//             $scope.view.create = true;
//             $scope.modal.show();
//         };
//
//         $scope.updateCategoryModal = function (data) {
//             $scope.view.update = true;
//             $scope.modal.show();
//             $scope.newObject = angular.copy(data);
//         };
//
//         $scope.closeCategory = function () {
//             $scope.modal.hide();
//             initCreateForm();
//             viewDefault();
//         };
//
//         $scope.sendMember = function (object) {
//             Api.create('/'+$scope.view.title+($scope.view.create?'/add':'/update'), object)
//             .then(function (result) {
//                 if(result.status==200 &&result.data!="") {
//                     $scope.modal.hide();
//                     list();
//                     initCreateForm();
//                     viewDefault();
//                 }
//             });
//
//         };
//
//         $scope.deleteMember = function (data) {
//             var confirmPopup = $ionicPopup.confirm({
//                     title: 'Delete '+$scope.view.title,
//                     template: 'Yakin mau dihapus?'
//                 });
//                 confirmPopup.then(function (res) {
//                     if (res) {
//                         Api.create('/'+$scope.view.title+'/delete', data)
//                         .then(function (result) {
//                             console.log(result); //Object {data: "3", status: 200, config: Object, statusText: "OK"}
//                             if(result.status==200 &&result.data!="") {
//                                 $scope.modal.hide();
//                                 list();
//                             }
//                         });
//                     }
//                 });
//
//         };
//
//
//         $ionicModal.fromTemplateUrl('add.html', {
//             scope: $scope,
//             animation: 'slide-in-up'
//         }).then(function (modal) {
//             $scope.modal = modal;
//         });
//     })
//
// ;
