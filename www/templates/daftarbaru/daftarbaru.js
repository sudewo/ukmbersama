angular.module('app')
.controller("daftarBaruCtrl", ['$scope', '$timeout','$rootScope','Api','$location','$ionicPopup','$state',
function ($scope, $timeout, $rootScope, Api, $location, $ionicPopup, $state)
{


    function index(){
        $scope.data = {};


    }

    function save(){
        $scope.data = {};
        $scope.data.name = '';
        $scope.data.email = '';
        $scope.data.password = '';
        // $scope.data.retypepassword = '';

        $scope.save = function () {
            // console.log($scope.data);
            $scope.error = {};
            $scope.submitted = true;
            Api.create('/auth/member/register',$scope.data).success(function(result){
                // console.log(result.data);
                var alertPopup = $ionicPopup.alert({
                    title: 'Registrasi Berhasil',
                    template: result.message
                });
                // $scope.modaldaftarbaru.hide();
                $scope.reset();
            }).error(function(result){
                // console.log(result);
                angular.forEach(result.data, function (error) {
                    $scope.error[error.field] = error.message;
//                            $scope.error[error.field] = $sce.trustAsHtml(error.message);
                });
            });
        };

        $scope.reset = function(){
          $scope.data = {};
          $scope.data.name = '';
          $scope.data.email = '';
          $scope.data.password = '';
          $scope.submitted = false;
          $scope.modaldaftarbaru.hide();
        }
    }

    index();
    save();

}]);
