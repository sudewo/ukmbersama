angular.module('app')
    .controller('listorderCtrl', function($scope,$ionicPopup,$window,$state, $rootScope,Api, $stateParams,$timeout,$ionicModal) {
        $scope.id = ($stateParams.id!='' && $stateParams.id !=undefined ? $stateParams.id : '');
        function viewDefault () {
            $scope.view = {
                title: ($scope.id==''? 'List Order' : 'Order Detail'),
                create: false,
                update: false,
                detail: ($scope.id==''? false : true),
            };
            $scope.field = {
                id:$scope.id,
//                action:'bayar',
                order_status:'',
                bayar:0,
                bayar_card:0,
                bayar_voucher:0,
                kembali:0
            };
        }
        viewDefault();
        
        
        
        
        function list () {
            Api.getAll('/orders/list', {
                member_id: $rootScope.currentUser.member_id,
                member_store_id: $rootScope.currentUser.member_store_id,
            }).then(function (result) {
                $scope.data = result.data.rows;
                if($scope.id!='' && $scope.data.length) {
                    $scope.data.forEach(function(i, j){
                        if(i.order_id == $scope.id && i.order_status == $rootScope.status.order.selesai.value) {
                            $state.transitionTo("menu.listorders");
                        }
                    });
                    listdetail();
                }
            });
        }
        list();
        
        function listdetail () {
            Api.getAll('/orders/listdetail', {
                member_id: $rootScope.currentUser.member_id,
                member_store_id: $rootScope.currentUser.member_store_id,
                order_id: $scope.id,
            }).then(function (result) {
                $scope.data_detail = result.data.rows;
                total_order();
            });
        }
        
        function total_order(){
            var total = 0;
            if($scope.data_detail.length) {
                $scope.data_detail.forEach(function(i, j){
                    total += parseInt(i.order_price);
                });
            }
            $scope.field.total_order = total;
        };
        
        // klik ke detail
        $scope.urlTo = function (data){
            $state.transitionTo("menu.listorders",{'id':data.order_id});
        };
        
        $scope.deleteModal = function (key) {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Delete',
                template: 'Yakin mau dihapus?'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    Api.delete('/orders/delete', key
                    ).then(function (result) {
                        if(result.data==1){
                            list();
                        } else {
                            alert('ada error');
                        }
                    });
                }
            });
        };
        
        $scope.updateModal = function (key) {
            $state.transitionTo("menu.orders",{'id':key.order_id});
        };
        
        $scope.changeStatus = function (item, status) { // ganti status
            $scope.field = {
                id:item.order_id,
                order_status:status
            };
            Api.create('/orders/update', $scope.field
                ).then(function (result) {
                    if(result.data.status==1){
                        list();
                    } else {
                        alert('ada error');
                    }
                });
        }
        
        $scope.kembalian = function (){
            $timeout(function (){
                var nominal = (($scope.field.bayar + $scope.field.bayar_card+ $scope.field.bayar_voucher) - $scope.field.total_order);
                $scope.field.kembali =  (nominal < 0 ? 0 : nominal);
            },100);
        };
        
        $scope.options = JSON.parse(JSON.stringify($rootScope.status.paybill));
        $scope.selection = [];

//        $scope.selectedMetode = function selectedMetode() {
//            return filterFilter($scope.options, { selected: true });
//        };
        $scope.selectedFruits = function selectedFruits() {
            return filterFilter($scope.options, { selected: true });
        };

        $scope.$watch('options|filter:{selected:true}', function (nv) {
            $scope.selection = nv.map(function (select) {
              return select.id;
            });
        }, true);
        
        $scope.toggleSelection = function (id) {
            var idx = $scope.selection.indexOf(id);
            
            // is currently selected
            if (idx > -1) {
                $scope.selection.splice(idx, 1);
            } else {
                $scope.selection.push(id);
            }
        }
        
        function validation() {
            console.log($scope.selection)
            if ($scope.selection === undefined || $scope.selection.length == 0) {
                alert('pilih jenis pembayaran');
                return false;
            }
            if($scope.selection.indexOf(2) > -1) {
                if($scope.field.no_card==undefined) {
                    alert('nomor kartu harus diisi'); 
                    return false;
                }
                if($scope.field.tid==undefined) {
                    alert('tid harus diisi'); 
                    return false;
                }
                if($scope.field.mid==undefined) {
                    alert('mid harus diisi'); 
                    return false;
                }
                if($scope.field.bayar_card==0) {
                    alert('nominal kartu harus diisi'); 
                    return false;
                }
                
            }
            if($scope.selection.indexOf(3) > -1) {
                if($scope.field.no_voucher==undefined) {
                    alert('nomor voucher harus diisi'); 
                    return false;
                }
                if($scope.field.bayar_voucher==0) {
                    alert('nominal voucher harus diisi'); 
                    return false;
                }
            }
            if($scope.field.kembali<0){
                alert('jumlah pembayaran masih kurang');
                return false;
            }
            return true;
        }
        
        $scope.openModalBayar = function() {
            // An elaborate, custom popup
            var myPopup = $ionicPopup.show({
              templateUrl: 'modal-transaksi.html',
              title: 'Pembayaran',
              subTitle: 'Please use normal things',
              scope: $scope,
              buttons: [
                { text: 'Cancel' },
                {
                  text: '<b>Save</b>',
                  type: 'button-positive',
                  attr: 'ng-disabled=false',
                  onTap: function(e) {
                    if (validation()) {
                        return $scope.field;
                    } else {
                        e.preventDefault();
                    }
                  }
                },
              ]
            });
            myPopup.then(function(res) {
                if(res!=undefined) {
                    $scope.field.action = 'bayar';
                    $scope.field.order_status = $rootScope.status.order.selesai.value;
                    Api.create('/orders/update', $scope.field
                    ).then(function (result) {
                        if(result.data.status==1){
                            $scope.field = {};
                            list();
                            viewDefault();
//                            $state.transitionTo("menu.products",null,{reload: true,inherit: false,notify: true});
//                            $state.transitionTo("menu.products");
                            $state.transitionTo("menu.listorders");
                        } else {
                            console.log('ada error');
                        }
                    });
                    
                } else {
//                    
                }
            });
   
        }
    })