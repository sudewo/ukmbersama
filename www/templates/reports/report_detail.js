angular.module('app')
.controller("reportsDetailCtrl", ['$scope', '$timeout','formatMoney','$rootScope','Api','$ionicModal','ionicDatePicker','moment','$stateParams',
function ($scope, $timeout, formatMoney, $rootScope, Api, $ionicModal, ionicDatePicker,moment,$stateParams)
{

    $scope.active = 'month';
    $scope.ds = '';
    $scope.de = '';
    $scope.grand_total = 0;

    function chart()
    {

      var params = $stateParams;

        var ds = null;
        if(typeof(params.ds) !== undefined){
          ds = params.ds;
        }else{
          alert('please make sure you have date start parameter');
          return false;
        }

        var data = {
          member_id: $rootScope.currentUser.member_id,
          member_store_id: $rootScope.currentUser.member_store_id,
          order_type: 'detail',
          ds: ds,
        };

        if(typeof(params.product_id) !== undefined){
          data.product_id = params.product_id;
        }



        Api.getAll('/orders/report',data).then(function (result) {
          $scope.labels = [];
          $scope.datachartOrder = [];
          $scope.serie = ['Order ID', 'Total Order'];

            var total_order = [];
            var total_price = [];
            $scope.dataOrders = result.data.rows;
            result.data.rows.forEach(function(i, j)
            {
              i.created_at = new Date(i.created_at);
               var d = new Date(i.created_at);

                 var dname = moment(d).format('ddd, DD MMM');

                      $scope.labels.push(dname);
                      $scope.serie.push(i.order_price);

                 total_order.push(i.order_id);
                 total_price.push(i.order_price);


            })

            $scope.datachartOrder.push(total_order);
            $scope.datachartOrder.push(total_price);
            console.log($scope.datachartOrder);
            console.log($scope.serie);
        });

        $scope.onClick = function (points, evt) {
          console.log(points, evt);
        };
    }

    // modal for detail order
    $scope.detailOrders = function(id)
    {
      $ionicModal.fromTemplateUrl('order-detail.html', { scope: $scope, animation: 'slide-in-up' })
      .then(function(modal) {
          $scope.OrderDetailModal = modal;
          $scope.order_id = id;
          Api.getAll('/orders/listdetail', {
              member_id: $rootScope.currentUser.member_id,
              member_store_id: $rootScope.currentUser.member_store_id,
              order_id: $scope.order_id,
          }).then(function (result) {
              $scope.data_detail_order = result.data.rows;
              $scope.OrderDetailModal.show();

              $scope.grandTotal = function(total){
                $scope.grand_total = $scope.grand_total + total;
              }
          });
      });
    }

    chart();

}])
.config(function (ionicDatePickerProvider) {
    var datePickerObj = {
      inputDate: new Date(),
      setLabel: 'Set',
      todayLabel: 'Today',
      closeLabel: 'Close',
      mondayFirst: false,
      weeksList: ["S", "M", "T", "W", "T", "F", "S"],
      monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
      templateType: 'popup',
      from: new Date(2012, 8, 1),
      to: new Date(2018, 8, 1),
      showTodayButton: true,
      dateFormat: 'dd MMMM yyyy',
      closeOnSelect: false,
      disableWeekdays: [6]
    };
    ionicDatePickerProvider.configDatePicker(datePickerObj);
  })
.config(['ChartJsProvider', function (ChartJsProvider) {
        // Configure all charts
        ChartJsProvider.setOptions({
          chartColors: ['#FF5252', '#FF8A80'],
          responsive: true
        });
        // Configure all line charts
        ChartJsProvider.setOptions('line', {
          showLines: false
        });
  }]);
