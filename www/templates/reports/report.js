angular.module('app')
.controller("reportsCtrl", ['$scope', '$timeout','formatMoney','$rootScope','Api','$ionicModal','ionicDatePicker','moment','$location','$ionicPlatform','$filter','$state',
function ($scope, $timeout, formatMoney, $rootScope, Api, $ionicModal, ionicDatePicker,moment, $location, $ionicPlatform, $filter, $state)
{
    $scope.active = 'day';
    $scope.ds = '';
    $scope.de = '';

    // $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
    //   console.log(fromState);
    //   if(fromState.name == "menu.reports-detail"){
    //     chart();
    //   }
    // });

    function chart()
    {
        var data = {
          member_id: $rootScope.currentUser.member_id,
          member_store_id: $rootScope.currentUser.member_store_id,
          r: $scope.active
        };

        if($scope.ds != ''){
          data.ds = moment($scope.ds).format("YYYY-MM-DD");
        }

        if($scope.de != ''){
          data.de = moment($scope.de).format("YYYY-MM-DD")
        }


        Api.getAll('/orders/report',data).then(function (result) {
          $scope.labels = [];
          $scope.datachartOrder = [];
          $scope.serie = ['Total Order', 'Total Sell'];

          // var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
          // var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

            var total_order = [];
            var total_price = [];
            $scope.dataOrders = result.data.rows;
            result.data.rows.forEach(function(i, j)
            {
              i.created_at = new Date(i.created_at);
               var d = new Date(i.created_at);
               var order_price = $filter('currency')(i.order_price,'IDR ', 0);

               if($scope.active == 'month'){
                 var mname = moment(d).format('MMMM');
                 if($scope.labels.indexOf(mname) == -1){
                      $scope.labels.push(mname);
                 }
                 total_order.push(i.total_order);
                 total_price.push(i.order_price);

               }else{
                 var dname = moment(d).format('ddd, DD MMM');
                 if($scope.labels.indexOf(dname) == -1){
                      $scope.labels.push(dname);
                      $scope.serie.push(order_price);
                 }
                 total_order.push(i.total_order);
                 total_price.push(i.order_price);
               }

            })

            $scope.datachartOrder.push(total_order);
            $scope.datachartOrder.push(total_price);
        });



        $scope.onClick = function (points, evt) {
          console.log(points, evt);
        };
    }


    // modal for detail order
    $ionicModal.fromTemplateUrl('order-detail.html', { scope: $scope, animation: 'slide-in-up',})
    .then(function(modal) {
        $scope.modalOrderDetail = modal;


        Api.getAll('/orders/listdetail', {
            member_id: $rootScope.currentUser.member_id,
            member_store_id: $rootScope.currentUser.member_store_id,
            order_id: $scope.id,
        }).then(function (result) {
            $scope.data_detail = result.data.rows;
        });
    });

    // modal for new order
    $ionicModal.fromTemplateUrl('order-setting.html', {
        scope: $scope,
        animation: 'slide-in-up',
    }).then(function(modal) {
        $scope.modalOrderSetting = modal;

        $scope.setActive = function(type) {
            $scope.active = type;
        };
        $scope.isActive = function(type) {
            return type === $scope.active;
        };

        $scope.message = {
           text: 'hello world!',
           time: new Date()
        };

        $scope.openDatePicker = function(data){
          var ipObj1 = {
            callback: function (val) {  //Mandatory
              var ds = new Date(val);

              if(data == 'start'){
                $scope.ds = moment(ds).format("DD MMM YYYY");
              }else{
                $scope.de = moment(ds).format("DD MMM YYYY");
              }

              console.log('Return value from the datepicker popup is : ' + val, new Date(val));
            },
            disabledDates: [            //Optional
              new Date(2016, 2, 16),
              new Date(2015, 3, 16),
              new Date(2015, 4, 16),
              new Date(2015, 5, 16),
              new Date('Wednesday, August 12, 2015'),
              new Date("08-16-2016"),
              new Date(1439676000000)
            ],
            from: new Date(2012, 1, 1), //Optional
            to: new Date(2016, 10, 30), //Optional
            inputDate: new Date(),      //Optional
            mondayFirst: true,          //Optional
            disableWeekdays: [0],       //Optional
            closeOnSelect: false,       //Optional
            templateType: 'popup'       //Optional
          };

          ionicDatePicker.openDatePicker(ipObj1);
        };

    });

    $scope.saveOrderSetting = function() {
        $scope.modalOrderSetting.hide();
        chart();
    };

    $scope.resetOrderSetting = function() {
      $scope.active = 'month';
      $scope.ds = '';
      $scope.de = '';
    }

    chart();

    $scope.orderDetails = function(date_start,active)
    {
        if(active == 'month'){
          date_start = moment(date_start).format('YYYY-MM');
        }else{
          date_start = moment(date_start).format('YYYY-MM-DD');
        }

        var obj = {'ds': date_start};
        $state.go('menu.reports-detail', obj);
    }

}])
.config(function (ionicDatePickerProvider) {
    var datePickerObj = {
      inputDate: new Date(),
      setLabel: 'Set',
      todayLabel: 'Today',
      closeLabel: 'Close',
      mondayFirst: false,
      weeksList: ["S", "M", "T", "W", "T", "F", "S"],
      monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
      templateType: 'popup',
      from: new Date(2012, 8, 1),
      to: new Date(2018, 8, 1),
      showTodayButton: true,
      dateFormat: 'dd MMMM yyyy',
      closeOnSelect: false,
      disableWeekdays: [6]
    };
    ionicDatePickerProvider.configDatePicker(datePickerObj);
  })
.config(['ChartJsProvider', function (ChartJsProvider) {
        // Configure all charts
        ChartJsProvider.setOptions({
          chartColors: ['#FF5252', '#FF8A80'],
          responsive: true
        });
        // Configure all line charts
        ChartJsProvider.setOptions('line', {
          showLines: false
        });
  }]);
