angular.module('app').controller('product.detail.controller', function ($rootScope, $scope, $stateParams, $location, Api, $state, $ionicTabsDelegate, fileUpload) {
    $scope.product = {};
    $scope.product_media = {};
    $rootScope.isNewRecord = true;
    var stateName = $state.current.name;
    console.log(stateName);
    switch (stateName) {
        case 'menu.productdetail':
            detailProduct();
            break;
        case 'menu.productupdate':
            $scope.isNewRecord = false;
            getCategory();
            updateProduct();
            break;
        default :
            console.log("state not found")
    }


    function detailProduct(){
        Api.getAll('/product/detail', {id: $stateParams.idx})
            .then(function (result) {
                $scope.detailProduct = result.data.rows[0];
            });
    }

    function updateProduct(){
        Api.getAll('/product/detail', {id: $stateParams.idx})
            .then(function (result) {
                //console.log(result.data.rows[0]['product_item_stock']);return false;
                $scope.product = result.data.rows[0];
                $scope.product.product_item_stock = parseInt(result.data.rows[0]['product_item_stock']);
                $scope.product.product_item_price = parseInt(result.data.rows[0]['product_item_price']);
                $scope.product.product_item_discount = parseInt(result.data.rows[0]['product_item_discount']);
                $scope.product_media.product_item_id = $scope.product.product_item_id;
                    console.log($scope.product);
            });
    }

    $scope.updateProduct = function(){
        Api.update('/productitem/update/'+$scope.product.product_item_id, $scope.product)
            .then(function (e) {
                console.log(e);
            });
        $ionicTabsDelegate.select(1);
    }


    function getCategory() {
        Api.getAll('/productcategories/list', {
            member_id: $rootScope.currentUser.member_id,
            member_store_id: $rootScope.currentUser.member_store_id,
        }).then(function (result) {
            $scope.data_category = result.data.rows;
            var addNewCategory = {'product_category_id' : '0', 'product_category_title': 'Add New Product'};
            $scope.data_category.push(addNewCategory);
        });
    };

    $scope.uploadFile = function () {
        //console.log($scope.product_media);
        var file = $scope.product_media.image;
        var uploadUrl = Api.apiUrl()+'/productitemmedia/add';
        var option = [
            {'product_item_id' : $scope.product_media.product_item_id},
            {'member_id' : 123},
            {'member_store_id' : 456},
            //{'member_id' : $scope.currentUser.member_id},
            //{'member_store_id' : $scope.currentUser.member_store_id},
        ];
        fileUpload.uploadFileToUrl(file, uploadUrl, option);
    }

});