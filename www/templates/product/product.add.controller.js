/**
 * Created by iwansusanto on 7/27/16.
 */
angular.module('app').controller('product.add.controller', function ($rootScope, $scope, Api, $ionicTabsDelegate, $ionicPopup, $cordovaCamera, $cordovaFile, fileUpload) {
    $scope.master = {};
    $scope.product = {};
    function getCategory() {
        Api.getAll('/productcategories/list', {
            member_id: $rootScope.currentUser.member_id,
            member_store_id: $rootScope.currentUser.member_store_id,
        }).then(function (result) {
            $scope.data_category = result.data.rows;
            var addNewCategory = {'product_category_id' : '0', 'product_category_title': 'Add New Product'};
            $scope.data_category.push(addNewCategory);
        });
    };
    getCategory();
    $scope.addProduct = function (product) {
        product.member_id = $rootScope.currentUser.member_id;
        product.member_store_id = $rootScope.currentUser.member_store_id;
        $scope.master = angular.copy(product);
        Api.create('/product/add', $scope.master).success(function (result) {
            //$scope.product = product;
            $scope.product_item_id_media = result.lastInsertId;
            console.log(result);
            $scope.product = angular.copy($scope.master);
        });
        $ionicTabsDelegate.select(1);
    };

    $scope.addNewCategory = function (data) {
        if(parseInt(data) <= 0){
            $scope.data = {};

            // An elaborate, custom popup
            var myPopup = $ionicPopup.show({
                template: '<input type="text" ng-model="data.product_category_title">',
                title: 'Enter New Category',
                scope: $scope,
                buttons: [
                    { text: 'Cancel' },
                    {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            if (!$scope.data.product_category_title) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                return $scope.data.product_category_title;
                            }
                        }
                    }
                ]
            });

            myPopup.then(function(res) {
                $scope.new_category = {};
                $scope.new_category.member_id = $rootScope.currentUser.member_id;
                $scope.new_category.member_store_id = $rootScope.currentUser.member_store_id;
                $scope.new_category.product_category_title = res;
                $scope.new_category.product_category_desc = res;
                $scope.new_category.product_category_image = res;
                $scope.new_category.product_category_parent = res;
                $scope.new_category.product_category_active = 1;
                Api.create('/productcategories/add', $scope.new_category).success(function (result) {
                    var addNewCategory = {'product_category_id' : result.lastInsertId, 'product_category_title': res};
                    $scope.data_category.push(addNewCategory);
                    //var xxx = {'product_category_id' : '0', 'product_category_title': 'Add New Product'};
                    //$scope.data_category.push(xxx);
                })
            });
        }else{
            return false;
        }
    };

    var isCordovaApp = !!window.cordova;

    $scope.uploadFile = function(media){
        var file = media.image;
        var uploadUrl = Api.apiUrl()+'/productitemmedia/add';
        var option = [
            {'product_item_id' : $scope.product_item_id_media},
            {'member_id' : 123},
            {'member_store_id' : 456},
            //{'member_id' : $scope.currentUser.member_id},
            //{'member_store_id' : $scope.currentUser.member_store_id},
        ];
        fileUpload.uploadFileToUrl(file, uploadUrl, option);
    };

    //PRODUCT MEDIA
    if(isCordovaApp){
    //    FOR MOBILE
        $scope.addMedia = function () {
            console.log("brow");
        }
    }else{
    //    FOR BROWSER
    }

    $rootScope.xxx = function (id) {
        console.log('click'+id);
    }


});