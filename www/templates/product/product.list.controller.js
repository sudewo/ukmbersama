/**
 * Created by Teguh kristianto on 7/27/16.
 */

angular.module('app').controller('product.list.controller', function ($scope, Api, $rootScope, $ionicTabsDelegate) {
    Api.getAll('/product/list', {
        member_id: $rootScope.currentUser.member_id,
        member_store_id: $rootScope.currentUser.member_store_id,
    }).then(function (result) {
        $scope.productsList = result.data.rows;
        console.log($scope.productsList);
        $ionicTabsDelegate.select(0);
    });
    $scope.default_i = 0;
    $scope.addNewProduct = function () {

    }
});
