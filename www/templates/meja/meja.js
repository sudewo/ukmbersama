angular.module('app')
    .controller('mejaCtrl', function($http,$scope, $ionicModal, $ionicPopup, $timeout,$stateParams,$window,$state,Api,DateFormat,$filter, $rootScope) {
        
        function initCreateForm() {
            $scope.field = '';
            $scope.field = {
                member_id: $rootScope.currentUser.member_id,
                member_store_id: $rootScope.currentUser.member_store_id,
                status:{},
                meja_no:{},
                label:{}
            };
        }
        initCreateForm();
        
        function list() {
            Api.getAll('/meja/list', {
                member_id: $rootScope.currentUser.member_id,
                member_store_id: $rootScope.currentUser.member_store_id,
            }).then(function (result) {
                $scope.data = result.data.rows;
                if($scope.data.length){
                    $scope.form = false;
                } else {
                    $scope.form = true;
                }
                getData();
            });
        }
        list();
        
        
        function getData(){
            $scope.data.forEach(function (i,j){
                $scope.field.meja_no[i.meja_id] = i.meja_no;
                $scope.field.label[i.meja_id] = i.label;
            });
        }
        $scope.saveJumlah = function () {
            
            if($scope.field.jumlah_meja != undefined) {
                Api.create('/meja/add', $scope.field
                    ).then(function (result) {
                        if(result.data.status==1){
                            initCreateForm();
                            list();
                        } else {
                            
                        }
                    });
            }
        };
        
        $scope.clickStatus = function (menu_id) {
            if($scope.field.status[menu_id]==undefined)
                $scope.field.status[menu_id] = 0;
        }
        
        $scope.editMeja = function () {
//            if($scope.field.label != undefined || $scope.field.status != undefined) {
                Api.create('/meja/update', $scope.field
                    ).then(function (result) {
                        if(result.data.status==1){
                            initCreateForm();
                            list();
                        } else {
                            
                        }
                    });
//            } else {
//            }
        };
        
        $scope.reset = function (){
            $scope.form = true;
            $scope.field.reset = true;
        }
    });
    