angular.module('app')
    .controller('resetCtrl', function($http,$scope, $ionicModal, $ionicPopup, $timeout,$stateParams,$window,$state,Api,DateFormat, $rootScope) {
        $scope.view = {
            title: 'Reset',
        };
        $scope.show_order = [];
        $scope.select_cek = [];
        $scope.data = [];
        
        function getData () {
            for ( var i = 0, len = localStorage.length; i < len; ++i ) {
                $scope.data.push({key:localStorage.key(i),value:localStorage.getItem(localStorage.key(i))})
            }
        }
        getData();
        
        $scope.checked = function(index) {
            if($scope.show_order.indexOf(index.key) !== -1){
                $scope.show_order.splice(index.key);
                
            }else{
                $scope.show_order.push(index.key)
            }    
        };
        $scope.select = function(index) {
            if($scope.select_cek.indexOf(index.key) !== -1){
                $scope.select_cek.splice(index.key);
            }else{
                $scope.select_cek.push(index.key)
            }
        };
        $scope.select_all = function () {
            if($scope.checked_all) {
                $scope.checked_all = false;
                $scope.select_cek=[];
            } else {
                $scope.checked_all = true;
                $scope.select_cek=[];
                $scope.data.forEach(function (i,j){
                    $scope.select_cek.push(i.key);
                });
            }
        };
        $scope.reset1 = function (){
//            console.log($scope.data)
            if($scope.select_cek.length) {
                var pilih = [];
                $scope.select_cek.forEach(function (i,j){
                    localStorage.removeItem(i);
                    $scope.data.forEach(function (k,l){
                        
                        if(k.key!=i){
                            pilih.push(k)
                        }
                    });
//                    $scope.data.splice(i);
                });
                $scope.data =pilih;
            } else {
                alert('Belum dipilih');
            }
        };
    });
    