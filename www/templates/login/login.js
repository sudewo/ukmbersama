angular.module('app')
.controller("loginCtrl", ['$scope', '$timeout','$rootScope','Api','$location','$ionicPopup','$state','$ionicModal',
function ($scope, $timeout, $rootScope, Api, $location, $ionicPopup, $state, $ionicModal)
{


    function login(){
        $scope.data = {};

        $scope.login = function () {
            Api.loginMember($scope.data.email, $scope.data.password).success(function(data){
                $state.go('menu.orders');
            }).error(function(data){
                console.log(data);
                var alertPopup = $ionicPopup.alert({
                    title: 'Login Gagal',
                    template: data
                });
            });
        };
        $scope.loggIn = function(){
          return true;
        }
    }


    function daftarbaru(){
        $ionicModal.fromTemplateUrl('templates/daftarbaru/daftarbaru.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modaldaftarbaru = modal;

        });

        $scope.opendaftarbaru = function(){
            $scope.modaldaftarbaru.show();
        }

        // $scope.cancelModal = function() {
        //     $scope.modaldaftarbaru.hide();
        // };
    }

    login();
    daftarbaru();

}]);
