angular.module('app')
    .controller('ordersCtrl', function($http,$scope, $ionicModal, $ionicPopup, $timeout,$stateParams,$window,$state,Api,DateFormat, $rootScope, $ionicPopover) {
        $scope.id = ($stateParams.id!='' && $stateParams.id !=undefined ? $stateParams.id : '');
        $scope.show_order = [];
        $scope.orders = [];

        $scope.view = {
            title: 'Orders',

        };

        function initCreateForm() {
            $scope.newObject = {
                order_id:$scope.id,
                member_id: $rootScope.currentUser.member_id,
                member_store_id: $rootScope.currentUser.member_store_id,
                order_transaction_number:"",
            };
        }
        initCreateForm();

        function order_item(index, qty) {
            return {
                product_item_id:index.product_item_id,
                product_category:index.product_category_id,
                product_item_stock:index.product_item_stock,
                order_qty:(qty==undefined?1:qty),
                product_item_price:index.product_item_price,
                product_title:index.product_title,
                product_item_name:index.product_item_name,
                media_title:index.media_title,
                sub_harga:1*index.product_item_price,
            }
        }

        function splice(index) {
            // $scope.show_order.splice($scope.show_order.indexOf(index.product_item_id), 1);
            $scope.show_order.forEach(function(value, j){
                // console.log(index);
                if(value == index.product_item_id){
                    // alert(value);
                    // $scope.orders.splice(value, 1); // remove index of object
                    $scope.show_order.splice(j, 1);
                    delete $scope.orders[value];
                    // $scope.orders[j] = {product_item_id:index.product_item_id}; // remove index of object
                    // $scope.orders[value] = order_item(index, (value==undefined? undefined : value.order_qty)); // remove index of object
                    // console.log(value);

                }
            });
            console.log($scope.orders);
            console.log($scope.show_order);
        }

        function push(index,i) {
            // $scope.orders = [];
            $scope.show_order.push(index.product_item_id);
            $scope.show_order.forEach(function(value, j){
                // alert(index.product_item_id);
                // $scope.orders.push(order_item(index, (value==undefined? undefined : value.order_qty)));

                if(value == index.product_item_id){
                  // alert('add');

                  // console.log($scope.orders);
                  // $scope.orders[value] = order_item(index, (value==undefined? undefined : value.order_qty)); // remove index of object
                  // $timeout(function(){
                      $scope.orders[value] = order_item(index, (value==undefined? undefined : value.order_qty)); // remove index of object
                  // }, 1000);
                }

            });
            console.log($scope.orders);
            console.log($scope.show_order);
            // return false;
            // return false;
            // console.log($scope.orders);
        }

        function getCategory () {
            function getProductCategoryList (productCategoryList) {

                var all = {product_category_desc:"Semuanya",product_category_id:""};

                $scope.data_category = JSON.parse(productCategoryList).data;
                $scope.data_category.splice(0, 0, all);
            }
            var productCategoryList = localStorage.getItem('/productcategories/list');
            if(productCategoryList==null || JSON.parse(productCategoryList).tanggal < DateFormat.getDate()){
                Api.getAll('/productcategories/list', {
                    member_id: $rootScope.currentUser.member_id,
                    member_store_id: $rootScope.currentUser.member_store_id,
                }).then(function (result) {
                    localStorage.setItem("/productcategories/list",JSON.stringify({tanggal:DateFormat.getDate(),data:result.data.rows}));
                    getProductCategoryList(localStorage.getItem('/productcategories/list'));
                });
            } else {
                getProductCategoryList(productCategoryList);
            }
        }

        function list() {
            function getProductList (productList) {
                $scope.data = [];
                // $scope.data = JSON.parse(productList).data;
                angular.forEach(JSON.parse(productList).data, function(value, i){
                      $scope.data[value.product_item_id] = value;
                });
                if($scope.id!='') {
                    list_order();
                }
                // console.log($scope.data);
                getCategory();
            }
            var productList = localStorage.getItem('/product/list');
            if(productList==null || JSON.parse(productList).tanggal < DateFormat.getDate()){
                Api.getAll('/product/list',{
                    member_id: $rootScope.currentUser.member_id,
                    member_store_id: $rootScope.currentUser.member_store_id,
                })
                .then(function (result) {
                    $scope.data = [];
                    $timeout(function(){
                      localStorage.setItem("/product/list",JSON.stringify({tanggal:DateFormat.getDate(),data:result.data.rows}));
                      angular.forEach(result.data.rows, function(value, i){
                            $scope.data[value.product_item_id] = value;
                      });
                    }, 100);
                    // console.log($scope.data);
                });
                getCategory();
            } else {
                getProductList(productList);
            }
        }
        list();

        function list_order() {
            Api.getAll('/orderdetail/list', {
                member_id: $rootScope.currentUser.member_id,
                member_store_id: $rootScope.currentUser.member_store_id,
                order_id: $scope.id,
            }).then(function (result) {
                $scope.list_order = result.data.rows;
                loadDetail();
            });
        }


        function cek_list_order () {
            Api.getAll('/orders/list', {
                member_id: $rootScope.currentUser.member_id,
                member_store_id: $rootScope.currentUser.member_store_id,
            }).then(function (result) {
                $scope.data_list_order = result.data.rows;
            });
        }

        function loadDetail() {
            $scope.list_order.forEach(function(i, j){
                $scope.data.forEach(function(k, l){
                    if(k.product_item_id == i.product_item_id) {
                        push(k,i);
                    }
                });
            });
            $scope.newObject.order_transaction_number = $scope.list_order[0].order_resi;
        }

        $scope.checked = function(index,status) {
            if($scope.show_order.indexOf(index.product_item_id) !== -1){
                if(parseInt(status) == 1) {
                    var confirmPopup = $ionicPopup.confirm({
                        title: 'Delete',
                        template: 'Yakin mau dihapus?'
                    });
                    confirmPopup.then(function (res) {
                        if (res) {
                            splice(index);
                        }
                    });
                } else {
                  // alert('as');
                  // console.log(index);
                    splice(index);
                }

            }else{
                var order_lama = false;
                if($scope.id!='') {
                    $scope.show_order.forEach(function(i, j){
                        if(i == index.product_item_id) {
                            push(index,i);
                            order_lama = true;
                        }
                    });
                }
                if(!order_lama){
                    push(index);
                }

            }
        };

        $scope.plus = function (item) {
            // console.log(item);
            $scope.orders.forEach(function(i, j){
                if(i.product_item_id == item.product_item_id){
                    if(parseInt(i.order_qty) >= parseInt(item.product_item_stock)) {
                      var alertPopup = $ionicPopup.alert({
                          title: 'Alert',
                          template: 'Stock Tersisa '+item.product_item_stock
                      });
                    } else {
                        // console.log(i);
                        i.order_qty++;
                        i.sub_harga = i.order_qty * item.product_item_price;
                    }
                }
            });
        };

        $scope.min = function (item) {
            $scope.orders.forEach(function(i, j){
                if(i.product_item_id == item.product_item_id){
                    if(parseInt(i.order_qty) <= 1) {
                      var alertPopup = $ionicPopup.alert({
                          title: 'Alert',
                          template: 'Minimal Order 1'
                      });
                    } else {
                        i.order_qty--;
                        i.sub_harga = i.order_qty * item.product_item_price;
                    }
                }
            });
        }

        $ionicModal.fromTemplateUrl('order.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modalorder = modal;

        });

        $scope.kategorichange = function(item){
            $scope.search = item.product_category_id;
        }

        $scope.sortproduct = [
            {text: 'termurah',value: 'price'},
            {text: 'hot item',value: '-sticky'},
        ];

        $ionicModal.fromTemplateUrl('my-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;

        });

        $scope.getTotal = function(){
            var total = 0;
            if($scope.show_order.length) {
                $scope.orders.forEach(function(i, j){
                    total += i.sub_harga;
                });
            }
            return total;
        }

        $scope.openModal = function() {
            $scope.modal.show();
        };

        $scope.cancelModal = function() {
            $scope.modal.hide();
            $scope.search = '';
        };

        $scope.applyModal = function() {
//    console.log($scope.choice);
//      return false;
            $scope.modal.hide();
        };

        $ionicModal.fromTemplateUrl('order.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.search = '';
            $scope.modalorder = modal;

        });

        $scope.openOrder = function()
        {
            // console.log($scope.show_order);
            // if(!angular.equals({}, $scope.orders)){
            if($scope.show_order.length) {
                $scope.modalorder.show();
            } else {
              var alertPopup = $ionicPopup.alert({
                  title: 'Alert',
                  template: 'Silahkan Masukkan Item Yang Akan di Order'
              });
            }
        };

        $scope.orderNow = function() {
            if($scope.show_order.length) {
                //cek bangku
                cek_list_order();
                $scope.order_member.show();
            } else {
              var alertPopup = $ionicPopup.alert({
                  title: 'Alert',
                  template: 'Silahkan Masukkan Item Yang Akan di Order'
              });
            }
        };

        $ionicModal.fromTemplateUrl('order-member.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.order_member = modal;
        });
        $scope.closeOrderMember = function () {
            $scope.order_member.hide();
        };

        $scope.saveOrderMember = function () {
//            cek_list_order();

            $timeout(function (){
                // cek kursi sudah ada atau belum
                var kursi_ada = false;

                $scope.data_list_order.forEach(function(i, j){
                    if(i.order_transaction_number==$scope.newObject.order_transaction_number && i.order_status!=$rootScope.status.order.selesai.value && $scope.id==''){
                        kursi_ada = true;
                    }
                });

                if(kursi_ada){
                    alert('Kursi sudah ada');
                }else if($scope.newObject.order_transaction_number!=''){
                    $scope.newObject.orders = JSON.stringify($scope.orders);
                    Api.create('/orders'+($scope.id!=''?'/update':'/add'), $scope.newObject
                    ).then(function (result) {
                        if(result.data.status==1){
                            $scope.order_member.hide();
                            $scope.modalorder.hide();
                            $scope.orders = [];
                            $scope.show_order = [];
                            initCreateForm();
                            $state.go("menu.listorders");
//                            $state.transitionTo("menu.products",null,{reload: true,inherit: false,notify: true});
//                            $state.transitionTo("menu.products");
//                            $state.transitionTo("menu.listorders");
                        } else {
                            console.log('ada error');
                        }
                    });
                } else {
                    alert('Kursi harus di isi...');
                }
            },200);
        };

        $scope.cancelModalOrder = function() {
            $scope.modalorder.hide();
            $scope.search = '';
        };

        $scope.applyModal = function() {
            $scope.modalorder.hide();
        };

        // Triggered on a button click, or some other target
        $scope.showPopup = function() {
            $scope.data = {}

            // An elaborate, custom popup
            var myPopup = $ionicPopup.show({
                templateUrl: 'popup-template.html',
                title: 'Sort Products',
                scope: $scope,
                buttons: [
                    { text: 'Cancel' },
                    {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            console.log($scope.data);
                            return $scope.data;
                        }
                    },
                ]
            });

            myPopup.then(function(res) {
                console.log('Tapped!', res);
                if(res !== undefined)
                    $scope.sort = res.sort;
            });

//   $timeout(function() {
//      myPopup.close(); //close the popup after 3 seconds for some reason
//   }, 3000);
        };

        $ionicPopover.fromTemplateUrl('templates/popover.html', {
          scope: $scope,
        }).then(function(popover) {
          $scope.popover = popover;
        });

        $scope.openPopover = function($event, index){
          $scope.itemdata = index;
          $scope.popover.show($event);
        };

        $scope.addOrder = function(index){
          var order_lama = false;
          if($scope.id!='') {
              $scope.show_order.forEach(function(i, j){
                  if(i == index.product_item_id) {
                      push(index,i);
                      order_lama = true;
                  }
              });
          };
          if(!order_lama){
              push(index);
          };

          $scope.popover.hide();
        };

        $scope.deleteOrder = function(index, status){
          if($scope.show_order.indexOf(index.product_item_id) !== -1){
              if(parseInt(status) == 1) {
                  var confirmPopup = $ionicPopup.confirm({
                      title: 'Delete',
                      template: 'Yakin mau dihapus?'
                  });
                  confirmPopup.then(function (res) {
                      if (res) {
                          splice(index);
                          $scope.popover.hide();
                      }
                  });
              } else {
                  splice(index);
              };

          };
        };

    });
