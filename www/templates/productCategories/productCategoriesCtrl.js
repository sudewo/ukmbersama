/**
 * Created by iwansusanto on 7/20/16.
 */
angular.module('app').controller('productCategoriesCtrl', function ($scope, $rootScope, $location, Api) {
    function getCategory() {
        Api.getAll('/productcategories/list', {
            member_id: $rootScope.currentUser.member_id,
            member_store_id: $rootScope.currentUser.member_store_id,
        }).then(function (result) {
            $scope.data_category = result.data.rows;
        });
    }
    getCategory();
    $scope.errors = [];
    $scope.product = {};
    $scope.addproduct = function (product) {
        product.member_id = $rootScope.currentUser.member_id;
        product.member_store_id = $rootScope.currentUser.member_store_id;
        if(product.product_category_id == null){
            $scope.errors.push({
                'field' : 'product_category_id',
                'errormsg' : 'Silahkan pilih kategori product'
            });
        }
        if(product.product_title == null){
            $scope.errors.push({
                'field' : 'product_title',
                'errormsg' : 'Inputkan nama product'
            });
        }
        if($scope.errors.length === 0){
            //Api.create('/product/add', product).success(function (result) {
            //    console.log(result);
            //    $scope.product = product;
            //});
            console.log($scope.product);
            $rootScope.$broadcast('sendProduct', $scope.product);
            $location.path('/side-menu21/productitems/add');
            $scope.errors = [];
        }else{
            alert(JSON.stringify($scope.errors));
            $scope.errors = [];
        }
    };


    $scope.productitem = {};
    $rootScope.$on('sendProduct', function(event, args) {
        $scope.productitem.item_name = args.product_title;
    });

    $scope.addProductItem = function (obj) {
        $scope.item = obj;
    }

    $scope.addNewCategory = function () {
    }
});